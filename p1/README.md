
# LIS4381

## Riley Morse

### Project #1 Requirements:


1. Running Business Card App
4. Skill set 7-9 screenshots

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface
* Screenshot of skillsets 7-9


#### Project Screenshots:

|Home Page|Information Page|
|:-------:|:---------:|
|![P1 Home Page](home.png)|![P1 Information Page](information.png)|

|Skill Set 7|Skill Set 8|Skill Set 9|
|:-------:|:---------:|:---------:|
|[![Skill Set 7](SS7.png)](SS7.png)|[![Skill Set 8](SS8.png)](SS8.png)|[![Skill Set 9](SS9.png)](SS9.png)|


