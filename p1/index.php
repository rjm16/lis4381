<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1.">
		<meta name="author" content="Riley Morse">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> For this project we designed a Business Card app so that we could promote ourselves in the IT industry. We also provided screenshots for skillsets 7-9. 
				</p>
				<p class="text-justify">
					<strong>Requirements:</strong> <ol><li>Business Card App</li><li>Skillsets 7-9</li></ol>
				</p>

				<h4>P1 Home Page</h4>
				<img src="home.png" class="img-responsive center-block" alt="A3 ERD">
				
				<h4>Information Page</h4>
				<img src="information.png" class="img-responsive center-block" alt="Unpopulated Home Page">

				<h4>Skillset 7</h4>
				<img src="SS7.png" class="img-responsive center-block" alt="Populated Home">

				<h4>Skillset 8</h4>
				<img src="SS8.png" class="img-responsive center-block" alt="Populated Home">

				<h4>Skillset 9</h4>
				<img src="SS9.png" class="img-responsive center-block" alt="Populated Home">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
