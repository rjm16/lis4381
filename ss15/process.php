<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
    <meta name="author" content="Your Name Here!">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - SS15</title>
        <?php include_once("../css/include_css.php"); ?>

    <style>
        #container{
           display: flex;
           flex-direction: column;
           justify-content: center;
           align-items: center;
           margin-top:150px;
        }

        #title{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-bottom: 1px solid;
            /*background-color: grey;*/
        }

        #text-container{
        	width:600px;
        }

    </style>
</head>
<body>
	<?php
		

	?>

    <?php include_once("../global/nav.php"); ?>
<div id="container">
    <div id="title">
      	<h1>Write/Read File</h1>
        <h4>Program writes to and reads from same file</h4>
    </div> 

    <p id="text-container">
	    <?php
	    	$myfile = fopen("file.txt", "w+") or exit("Unable to open file!");
	    	$txt = $_POST['text'];
	    	fwrite($myfile, $txt);
	    	fclose($myfile);

	    	$myfile = fopen("file.txt", "r+")or exit("Unable to open file!");
	    	while(!feof($myfile)){
	    		echo fgets($myfile) . "<br />";
	    	}

	    	fclose($myfile);
		?>
	</p>
</div>
                    

</body>

</html>
