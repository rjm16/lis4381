<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
    <meta name="author" content="Your Name Here!">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - SS15</title>
        <?php include_once("../css/include_css.php"); ?>

    <style>
        #container{
           display: flex;
           flex-direction: column;
           justify-content: center;
           align-items: center;
           margin-top:150px;
        }

        #title{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-bottom: 1px solid;
            /*background-color: grey;*/
        }

        #data{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        #textbox{
            height:200px;
            width:600px;
            outline: none;
            resize: none;
        }

    </style>
</head>
<body>

    <?php include_once("../global/nav.php"); ?>
    <div id="container">
        <div id="title">
            <h1>Write/Read File</h1>
            <h4>Program writes to and reads from same file</h4>
        </div> 
        <form method="post" action="process.php"> 
            <div id="data">
                <h3>File Data</h3>
                <h5><textarea id="textbox" name="text" placeholder="Input text here..."></textarea></h5>
                <input type="submit" name="submit"/>
            </div> 
        </form>   
    </div>


                    

</body>

</html>
