> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Riley Morse

### Assignment #1 Requirements:


1. Ampps installation
2. Java installation and HelloWorld.java
3. Android Studio installation and creation of My First App
4. Git Commands with short descriptions
5. Bitbucket tutorial repository setup

#### README.md file should include the following items:

* Screenshot of ampps installtion
* Screenshot of running Java Hello World
* Screenshot of Android Studio My First App
* Git Commands with short descriptions
* Bitbucket repo links


>
> #### Git commands w/short descriptions:

1. git init -create new repository
2. git status - status of your repository
3. git add - add files to be committed
4. git commit - commit changes to your repository
5. git push - push committed files to remote repository
6. git pull - pulls changes from remote repository to local machine
7. git clone - copy a repository from a remote source


#### Assignment Screenshots:
AMPPS | JAVA | ANDROID STUDIO

![alt](ampps1.png) | ![alt](HelloWorld.png) | ![alt](FirstApp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rjm16/bitbucketstationlocations/src/master/  "Bitbucket Station Locations")

