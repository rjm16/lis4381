
# LIS4381

## Riley Morse

### Assignment #5 Requirements:


1. index.php shows table that displays db information
2. buttons for edit and delete on table rows
3. Add_petstore page, placeholders describe what must be inserted in input boxes.
4. add_petstore_process page, processes information inputted on add_petstore, validates information and ensures the necessary boxes were filled. Prompts error if boxes are left blank.
5. Skillsets 13-15
6. Web app link

#### README.md file should include the following items:

* Screenshot of Populated Index table
* Error when trying to add empty input boxes on the process page.
* Skillsets 13-15
* Web app http://localhost/repos/lis4381/


#### Assignment Screenshots:

|Index Page|Error|
|:-------:|:---------:|
|![A5 index](table.png)|![A5 Error](errorphp.png)|

|Skill Set 13|Skill Set 14 One|
|:-------:|:-------:|
|![Skill Set 13](ss13.png)|![Skill Set 14 one](SS14o.png)|

Skill Set 14 Two|Skill Set 14 Division|Skill Set 14 Zero|
|:-------:|:-------:|
![Skill Set 14 Two](SS14t.png)|![Skill Set 14 Divide](SS14d.png)|![Skill Set 14 Divide by Zero](SS14dn.png)|


|Skill Set 15 Write|Skill Set 15 Read|
|:-------:|:-------:|
|![Skill Set 15 Write](ss151.png)|![Skill Set 15 Read](SS152.png)|