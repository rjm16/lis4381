
# LIS4381

## Riley Morse

### Assignment #3 Requirements:


1. Running Event App
2. Screenshot of ERD
3. Links to following files- a3.mwb, a3.sql
4. Skill set 4-6 screenshots

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface
* Screenshot of skillsets 4-6
* Screenshot of ERD

### Links to .mwb and .sql

*[A3.mwb](a3.mwb)
*[A3.sql](a3.sql)


#### Assignment Screenshots:

|Unpopulated Page|Populated Page|ERD|
|:-------:|:---------:|:-------:|
|![A3 Unpopulated Page](unpopulated.png)|![A3 Populated Page](populated.png)|![A3 ERD](a3ERD.png)|

|Skill Set 4|Skill Set 5|Skill Set 6|
|:-------:|:---------:|:---------:|
|[![Skill Set 4](SS4.png)](SS4.png)|[![Skill Set 5](SS5.png)](SS5.png)|[![Skill Set 6](SS6.png)](SS6.png)|


