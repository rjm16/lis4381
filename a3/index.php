<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My Event App.">
		<meta name="author" content="Riley Morse">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> For this assignment we created an Event app that lets the user pick the event they are going to and how many tickets they need, and the app outputs the total price of the tickets. 
				</p>
				<p class="text-justify">
					<strong>Requirements:</strong> <ol><li>My Event App</li><li>A3 Mwb</li><li>Skillsets 4-6</li></ol>
				</p>

				<h4>A3 ERD</h4>
				<img src="a3ERD.png" class="img-responsive center-block" alt="A3 ERD">
				
				<h4>Unpopulated Home Page</h4>
				<img src="unpopulated.png" class="img-responsive center-block" alt="Unpopulated Home Page">

				<h4>Populated Home Screen</h4>
				<img src="populated.png" class="img-responsive center-block" alt="Populated Home">

				<h4>SS1</h4>
				<img src="SS4.png" class="img-responsive center-block" alt="Skill Set 4">

				<h4>SS2</h4>
				<img src="SS5.png" class="img-responsive center-block" alt="Skill Set 5">

				<h4>SS3</h4>
				<img src="SS6.png" class="img-responsive center-block" alt="Skill Set 6">

				<h4><a href="a3.mwb">A3 MWB</a></h4>
				<h4><a href="a3.sql">A3 SQL</a></h4>

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
