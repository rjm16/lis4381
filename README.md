> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381

## Riley Morse

### Class Number Requirements:

*LIS4381 Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App 	
    - Provide Screenshots of installation
    - Create Bitbucket repo
    - Complete bitbucket tutorial
    - Provide git command discriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Healthy Recipes App
    - Button, Change background and text color
    - Skillsets 1-3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - My Event App
    - A3 .mwb and .sql
    - Skillsets 4-6
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - My Business Card App
    - Skillsets 7-9
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Bootstrap Carousel
    - Screenshots for carousel, data validation fail and pass
    - Skillsets 10-12
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Database basic server side validation
    - screenshots of populated table, error page
    - Skillsets 13-15
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Edit/ Delete functionality for A5 table
    - RSS Feed