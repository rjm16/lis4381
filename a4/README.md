
# LIS4381

## Riley Morse

### Assignment #4 Requirements:


1. Create Bootstrap carousel
2. Create a for to collect data
3. Have data validated Client-side
4. Skill set 10-12 screenshots

#### README.md file should include the following items:

* Screenshot of Home Page
* Screenshot of Failed Data Validation
* Screenshot of Successful Data Validation
* Screenshot of Skillsets 10-12
* Web app http://localhost/repos/lis4381/


#### Assignment Screenshots:

|Home Page|Failed|Successful|
|:-------:|:---------:|:-------:|
|![A4 Home Page](portal.png)|![A4 Failed](failed.png)|![A4 Passed](passed.png)|

|Skill Set 10|Skill Set 11|Skill Set 12|
|:-------:|:---------:|:---------:|
|[![Skill Set 10](SS10.png)](SS10.png)|[![Skill Set 11](SS11.png)](SS11.png)|[![Skill Set 12](SS12.png)](SS12.png)|


