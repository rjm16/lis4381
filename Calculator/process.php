

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
    <meta name="author" content="Your Name Here!">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - Simple Calculator</title>
        <?php include_once("../css/include_css.php"); ?>

    <style>
        #container{
           display: flex;
           flex-direction: column;
           justify-content: center;
           align-items: center;
           margin-top:150px;
        }

        #title{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-bottom: 1px solid;
            /*background-color: grey;*/
        }


    </style>
</head>
<body>
	<?php
		$flag = 1;

		$num1 = $_POST['num1'];
		$num2 = $_POST['num2'];
		$option = $_POST['radio'];

		if($option == "division" && $num2 == 0){
			$flag = 0;
		}else{
			if($option == "addition"){
				$result = $num1 + $num2;
				$operator = '+';
			}else if($option == "subtraction"){
				$result = $num1 - $num2;
				$operator = '-';
			}else if($option == "multiplication"){
				$result = $num1 * $num2;
				$operator = '*';
			}else if($option == "division"){
				$result = $num1 / $num2;
				$operator = '/';
			}else if($option == "exponentiation"){
				$result = $num1**$num2;
				$operator = '^';
			}
		}		

	?>



    <?php include_once("../global/nav.php"); ?>
    <div id="container">
        <div id="title">
            <h1>Simple Calculator</h1>
            <h4>Performs addition, subtraction, multiplication, division, and exponentiation</h4>
        </div>


    
    <?php 
    	if($flag == 0){
    		echo "<p><h1>".$option."</h5></p>";
    		echo "<p><h3>Cannot divide by zero!</h3></p>";
    	}else{
    		echo "<p><h1>".$option."</h5></p>";
    		echo "<p><h3>".$num1." ".$operator." ".$num2." = ".$result."</h3></p>";
    	}
    	

    ?>
       
    </div>

</body>

</html>