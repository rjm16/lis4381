<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
    <meta name="author" content="Your Name Here!">
    <link rel="icon" href="favicon.ico">

    <title>LIS4381 - Simple Calculator</title>
        <?php include_once("../css/include_css.php"); ?>

    <style>
        #container{
           display: flex;
           flex-direction: column;
           justify-content: center;
           align-items: center;
           margin-top:150px;
        }

        #title{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            border-bottom: 1px solid;
            /*background-color: grey;*/
        }

        #calculate{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-top: 20px;
        }

        #radio{
           margin-top: 5px;
        }

        #buttons{
            margin-left: 10px;
        }

        #submitBtn{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }


    </style>
</head>
<body>

    <?php include_once("../global/nav.php"); ?>
    <div id="container">
        <div id="title">
            <h1>Simple Calculator</h1>
            <h4>Performs addition, subtraction, multiplication, division, and exponentiation</h4>
        </div> 
    <form method="post" action="process.php">
        <div id="calculate">
            <h3>Perform Calculation</h3>
            <h5>Num1:   <input type="text" name="num1"/></h5>
            <h5>Num2:   <input type="text" name="num2"/></h5>
        </div>  
        <div id="radio">
            <input type="radio" id="buttons" name="radio" value="addition">
            <label for="radio">Addition</label>
            <input type="radio" id="buttons" name="radio" value="subtraction">
            <label for="radio">Subtraction</label>
            <input type="radio" id="buttons" name="radio" value="multiplication">
            <label for="radio">Multiplication</label>
            <input type="radio" id="buttons" name="radio" value="division">
            <label for="radio">Divide</label>
            <input type="radio" id="buttons" name="radio" value="exponentiation">
            <label for="radio">Exponentiation</label>

        </div> 
        <div id="submitBtn">
            <h5><input type="submit" value="Calculate"/></h5>
        </div>
    </form>

        

    </div>



                    

</body>

</html>
