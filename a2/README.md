> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Riley Morse

### Assignment #2 Requirements:


1. Running Healthy Recipes App
2. Change Background color and text color
3. Button that brings up second user interface
4. Screenshots of skillsets 1-3

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface
* Screenshot of skillsets 1-3


#### Assignment Screenshots:

|Home Page|Recipe Page|
|:-------:|:---------:|
|![A2 Home Page](A2Home.png)|![A2 Recipe Page](A2Second.png)|

|Skill Set 1|Skill Set 2|Skill Set 3|
|:-------:|:---------:|:---------:|
|[![Skill Set 1](SS1.png)](SS1.png)|[![Skill Set 2](SS2.png)](SS2.png)|[![Skill Set 3](SS3.png)](SS3.png)|

