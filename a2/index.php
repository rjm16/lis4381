<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 2">
		<meta name="author" content="Riley Morse">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> For our 2nd Assignment, we created an app called Healthy Recipes, where we changed our apps background color, included and image, and a button that took you to a second screen! We also showed off our first 3 java skillsets.
				</p>
				<p class="text-justify">
					<strong>Requirements:</strong><ol><li>Healthy Recipes App</li><li>Button that changes screens</li><li>Skillsets 1-3</li> </ol>
				</p>



				<h4>A2 Home Page</h4>
				<img src="A2Home.png" class="img-responsive center-block" alt="A2 Home Page">

				<h4>A2 Second Screen</h4>
				<img src="A2Second.png" class="img-responsive center-block" alt="A2 Second Screen">

				<h4>SS1</h4>
				<img src="SS1.png" class="img-responsive center-block" alt="Skill Set 1">

				<h4>SS2</h4>
				<img src="SS2.png" class="img-responsive center-block" alt="Skill Set 2">

				<h4>SS3</h4>
				<img src="SS3.png" class="img-responsive center-block" alt="Skill Set 3">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
