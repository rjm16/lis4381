
# LIS4381

## Riley Morse

### Project 2 Requirements:


1. Edit and Delete functionality added to petstore table
2. delete gives user a prompt to confirm
3. edit process page pulls current data of entry
4. RSS feed of student's choice

#### README.md file should include the following items:

* Screenshot of homepage
* Screenshot of index.php
* Screenshot of editing
* Screenshot of editing error
* Screenshot of edited index
* Screenshot of delete prompt
* Screenshot of successful delete
* Screenshot of RSS feed.


#### Assignment Screenshots:

|Home Page|Index|
|:-------:|:---------:|
|![P2 Home](home.png)|![P2 Index](index.png)|

|Edit Page|Error|Passed Validation|
|:-------:|:-------:|:-------:
|![Edit Page](editpet.png)|![Error](error.png)|![Passed Validation](valid.png)|

Delete Prompt|Completed Delete|RSS Feed|
|:-------:|:-------:|
![Delete Prompt](prompt.png)|![Completed Delete](deleted.png)|![RSS Feed](rss.png)|

